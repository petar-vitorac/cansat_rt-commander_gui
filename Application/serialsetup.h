#ifndef SERIALSETUP_H
#define SERIALSETUP_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class SerialSetup; }
QT_END_NAMESPACE

class SerialSetup : public QDialog {
    Q_OBJECT

public:
    explicit SerialSetup(QWidget *parent = nullptr);
    ~SerialSetup();

private slots:
    void on_connectButton_clicked();

    void on_serialNameInput_returnPressed();

private:
    bool runSetup();
    bool setupInProgress = false;
    Ui::SerialSetup *ui;
};

#endif // SERIALSETUP_H
