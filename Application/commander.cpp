#include "commander.h"
#include <QSerialPort>
#include <QDebug>
#include <QObject>
#include <QVector>
#include "config.h"

Commander::Commander(QSerialPort *initializedSerial, QObject *parent) :
    QObject(parent),
    serial(initializedSerial),
    checkNewDataTimer(this)
{
    connect(&checkNewDataTimer, &QTimer::timeout, this, &Commander::checkNewData);
    checkNewDataTimer.start(checkNewDataCycle);
}

bool Commander::sendCommand(CansatCmdRequestType command, QVector<char*> parameters) {
    char strCommand[5]; // no command is ever longer than 5 characters
    snprintf(strCommand, 5, "%d", (int)command); // convert command number to string

    if(serial->write(strCommand) == -1) {
        err << "Error writing to serial: " << serial->errorString() << "\n";
        return false;
    }

    for(char* param : parameters) {
        if(serial->write(",") == -1 || serial->write(param) == -1) {
            err << "Error writing to serial: " << serial->errorString() << "\n";
            return false;
        }
    }

    serial->flush();

    return true;
}

void Commander::checkNewData() {
    while(serial->canReadLine()) {
        char data[1024];
        qint64 lineLength = serial->readLine(data, 1024);
        if(lineLength != -1) {
            emit receivedData(data);
        }
    }
}
