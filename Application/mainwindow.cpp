#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "serialsetup.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    serialSetup(new SerialSetup)
{
    ui->setupUi(this);

    setCentralWidget(serialSetup);
}

MainWindow::~MainWindow()
{
    delete ui;
}
