#include "serialsetup.h"

#include "window.h"
#include "ui_window.h"


Window::Window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Window)
{
    ui->setupUi(this);

    SerialSetup* ss = new SerialSetup(this);

    layout()->addWidget(ss);
}

Window::~Window()
{
    delete ui;
}
