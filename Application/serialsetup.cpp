#include "serialsetup.h"
#include "ui_serialsetup.h"

#include <QDebug>

SerialSetup::SerialSetup(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SerialSetup)
{
    ui->setupUi(this);
}

SerialSetup::~SerialSetup()
{
    delete ui;
}


void SerialSetup::on_connectButton_clicked()
{
    runSetup();
}

void SerialSetup::on_serialNameInput_returnPressed()
{
    runSetup();
}

bool SerialSetup::runSetup() {
    setupInProgress = true;
    qInfo() << "Button clicked";
    return true;
}
