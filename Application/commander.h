#ifndef COMMANDER_H
#define COMMANDER_H

#include <QObject>
#include <QSerialPort>
#include <QString>
#include <QTextStream>
#include <QTimer>
#include <QVector>
#include "..\cansat-asgard\libraries\cansatAsgardCSPU\src\CansatInterface.h"

class Commander : public QObject
{
    Q_OBJECT
public:
    explicit Commander(QSerialPort *initializedSerial, QObject *parent = nullptr);
    bool sendCommand(CansatCmdRequestType command, QVector<char*> parameters);

signals:
    void receivedData(char* data);

private slots:
    void checkNewData();

private:
    QSerialPort *serial;

    QTimer checkNewDataTimer;
};

#endif // COMMANDER_H
